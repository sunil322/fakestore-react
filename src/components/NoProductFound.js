import React, { Component } from 'react';
import './NoProductFound.css';

class NoProductFound extends Component {
  render() {
    return (
      <div className='message-div'>
        <h1>No Product Found</h1>
      </div>
    )
  }
}

export default NoProductFound;