import React, { Component } from 'react';
import './Products.css';
import Product from './Product';

class Products extends Component {
  render() {
    return (
      <div className='products-container'>
        {
          this.props.productsData.map((product)=>{
            return(
              <Product key={product.id} product={product}/>
            )
          })
        }
      </div>
    )
  }
}

export default Products;

