import React, { Component } from 'react';
import './Navbar.css';
import logo from '../images/logo.png';

class Navbar extends Component {
  render() {
    return (
      <>
        <nav>
          <div className='nav-logo'>
            <img src={logo} alt="logo"/>
          </div>
          <div className='nav-menu'>
            <ul>
              <li>SIGNUP</li>
            </ul>
          </div>
        </nav>
      </>
    )
  }
}

export default Navbar;