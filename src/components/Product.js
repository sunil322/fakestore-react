import React, { Component } from 'react'
import './Products.css';

class Product extends Component {
    render() {
        return (
            <div className='product'>
                <div className='product-img'>
                    <img src={this.props.product.image} alt="product" />
                </div>
                <div className='product-info'>
                    <p className='category'>{this.props.product.category}</p>
                    <p className='title'>{this.props.product.title}</p>
                    <p className='price'>Price : <span>${this.props.product.price}</span></p>
                    <p className='rating'><span><i className="fa-solid fa-star"></i> {this.props.product.rating.rate} ({this.props.product.rating.count})</span></p>
                </div>
                <div className='button-div'>
                    <button><i className="fa-sharp fa-solid fa-cart-plus"></i> Add to cart</button>
                </div>
            </div>
        )
    }
}

export default Product;