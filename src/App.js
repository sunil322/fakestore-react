import React, { Component } from "react";
import './App.css';
import Products from "./components/Products";
import Loader from "./components/Loader";
import Error from "./components/Error";
import Navbar from "./components/Navbar";
import NoProductFound from "./components/NoProductFound";
import Footer from "./components/Footer";

class App extends Component {

  constructor(props) {

    super(props);

    this.API_STATES ={
      LOADING:'loading',
      LOADED:'loaded',
      ERROR:'error'
    }

    this.state = {
      status: this.API_STATES.LOADING,
      productsData: []
    }
  }

  componentDidMount() {
    fetch('https://fakestoreapi.com/products')
      .then((res)=>{
        return res.json();
      })
      .then((data)=>{
        this.setState({
          productsData:data,
          status:this.API_STATES.LOADED
        })
      })
      .catch((err)=>{
        this.setState({
          status:this.API_STATES.ERROR
        })
      })  
  }

  render() {
    return (
      <>
      <Navbar/>
      {
        this.state.status === this.API_STATES.LOADING ?
        <Loader/> :
        this.state.productsData.length === 0 && this.state.status === this.API_STATES.LOADED ? 
        <NoProductFound/> :
        <Products productsData={this.state.productsData}/>
      }
      {
        this.state.status === this.API_STATES.ERROR ?
        <Error/> :
        <></>
      }
      <Footer/>
      </>
    )
  }
}

export default App;
